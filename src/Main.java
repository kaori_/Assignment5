public class Main {

    public static void main(String[] args) {

        int n = 5;

        //Problem1
        Factorial f = new Factorial();
        System.out.println("calculate Fractorial");
        System.out.println(f.factorial(n));

        //Problem2
        n = 10;
        Fibonacci fn = new Fibonacci();
        System.out.println("calculate Fibonacci");
        System.out.println(fn.fibonacci(n));

        //Problem3
        n = 12;
        if (PrimalityChecker.isPrime(n)) {
            System.out.println(n + " is a prime number.");
        } else {
            System.out.println(n + " is not a prime number.");
        }

        //Problem4
        n = 8;
        GoldbackChecker gc = new GoldbackChecker();
        int[] goldback = gc.checkGoldbach(n);
        if (goldback != null) {
            System.out.println(n + "'s goldbacks are " + goldback[0] + " " + goldback[1]);
        } else {
            System.out.println("There is no goldback.");
        }

    }
}
