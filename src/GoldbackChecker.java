public class GoldbackChecker {
    int[] checkGoldbach(int n) {
        if ((n <= 4) || (n%2 != 0)) {
            return null;
        }

        for (int i=2; i<(n/2)+1; i++) {
            if (PrimalityChecker.isPrime(i) && PrimalityChecker.isPrime(n-i)) {
                int result[] = new int[2];
                result[0] = i;
                result[1] = n-i;
                return result;
            }
        }
        return null;
    }
}
